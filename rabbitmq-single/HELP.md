# SpringBoot+RabbitMQ  单数据源 Demo

Springboot 集成 RabbitMQ 单MQ Demo


需要注意 :消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理