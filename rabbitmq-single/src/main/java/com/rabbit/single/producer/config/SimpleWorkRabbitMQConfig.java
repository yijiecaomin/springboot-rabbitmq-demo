package com.rabbit.single.producer.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 点对点 模式 配置
 *  简单模式     work工作模式
 *
 * @Author su
 * @Date 2022/7/21 15:50
 */
@Configuration
public class SimpleWorkRabbitMQConfig {

    /**
     * 简单队列  一个发布者 ->队列 -> 一个消费者
     *
     *  一个消费者消费队列中消息
     *
     * @return
     */
    @Bean
    public Queue simpleQueue(){
        return new Queue("simple.demo",true);
    }

    /**
     * 工作队列 : 一个发布者 -> 队列 -> 多个消费者
     * 工作模式与简单模式 只是消费者的数量问题 比如说将 队列当做一个快递站  消费者是快递员 将消息分发给大家来处理
     *
     * @return
     */
    @Bean
    public Queue workQueue(){
        return new Queue("work.demo",true);
    }

}
