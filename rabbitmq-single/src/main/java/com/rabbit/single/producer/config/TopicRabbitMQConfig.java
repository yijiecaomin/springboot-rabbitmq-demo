package com.rabbit.single.producer.config;

import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * topic 模式
 *
 * @Author su
 * @Date 2022/7/22 10:01
 */
@Configuration
public class TopicRabbitMQConfig {

    /**
     * topic 交换机
     *
     * @return
     */
    @Bean
    public TopicExchange getTopicExchange() {
        return ExchangeBuilder.topicExchange("topic.exchange.demo").durable(true).build();
    }
}
