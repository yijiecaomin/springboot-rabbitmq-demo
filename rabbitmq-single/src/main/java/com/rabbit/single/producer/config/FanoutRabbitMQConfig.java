package com.rabbit.single.producer.config;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 广播模式创建交换机
 *
 * @Author su
 * @Date 2022/7/21 17:43
 */
@Configuration
public class FanoutRabbitMQConfig {

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("fanout.exchange.demo", true, false);
    }
}
