package com.rabbit.single;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * RabbitMQ 单MQ 的demo项目
 *
 *
 *
 * direct：如果路由键匹配，则直接投递到对应的队列
 *
 * fanout：不处理路由键，向所有与之绑定的队列投递消息
 *
 * topic：处理路由键，按模式匹配，向符合规则的队列投递消息
 *
 * headers：允许匹配消息的header，而非路由键，除此之外，direct完全一致，但性能差很多，基本不用了。

 *
 */
@SpringBootApplication
public class RabbitmqSingleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqSingleApplication.class, args);
    }

}
