package com.rabbit.single.consumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 广播模式 消费者
 *
 * @Author su
 * @Date 2022/7/21 17:52
 */
@Component
public class FanoutMQConsumer {

    /**
     * 广播模式 消费者1号
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "fanout.queue.demo"), exchange = @Exchange(value = "fanout.exchange.demo", type = ExchangeTypes.FANOUT)))
    public void fanoutMsg(String msg) {
        System.out.println("广播模式 1号 接收到消息 :" + msg);
    }

    /**
     * 广播模式 消费者2号
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "fanout.queue.demo2"), exchange = @Exchange(value = "fanout.exchange.demo", type = ExchangeTypes.FANOUT)))
    public void fanoutMsg2(String msg) {
        System.out.println("广播模式 2号 接收到消息 :" + msg);
    }
}
