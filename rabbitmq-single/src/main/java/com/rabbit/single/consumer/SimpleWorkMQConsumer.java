package com.rabbit.single.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 工作模式 /简单模式 消费者
 * <p>
 * 需要注意 :消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
 *
 * @Author su
 * @Date 2022/7/21 16:06
 */
@Component
public class SimpleWorkMQConsumer {

    /**
     * 简单模式 消费者
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(queues = "simple.demo")
    public void simpleMsg(String msg) {
        System.out.println("简单模式 接收到消息 :" + msg);
          //int i = 1 / 0;
        //消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
    }

    /**
     * 工作模式 消费者1号
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(queues = "work.demo")
    public void workMsg1(String msg) {
        System.out.println("工作模式 1号工作者接收到消息 :" + msg);
    }

    /**
     * 工作模式 消费者2号
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(queues = "work.demo")
    public void workMsg2(String msg) {
        System.out.println("工作模式 2号工作者接收到消息 :" + msg);
    }
}
