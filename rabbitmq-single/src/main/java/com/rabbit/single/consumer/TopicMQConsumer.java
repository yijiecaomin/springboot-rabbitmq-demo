package com.rabbit.single.consumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 工作模式 /简单模式 消费者
 * <p>
 * 需要注意 :消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
 *
 * @Author su
 * @Date 2022/7/21 16:06
 */
@Component
public class TopicMQConsumer {

    /**
     * 简单模式 消费者  TODO 可以收到 #多个单词模糊匹配   发送者的路由是:topic.demo
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue("topic.queue.demo1"), exchange = @Exchange(name = "topic.exchange.demo", type = ExchangeTypes.TOPIC), key = "topic.#"))
    public void simpleMsg1(String msg) {
        System.out.println("topic模式 topic.# 接收到消息 :" + msg);
        //int i = 1 / 0;
        //消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
    }

    /**
     * 简单模式 消费者  //TODO 可以收到 单个单词 *模糊匹配   发送者的路由是:topic.demo
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue("topic.queue.demo2"), exchange = @Exchange(name = "topic.exchange.demo", type = ExchangeTypes.TOPIC), key = "topic.*"))
    public void simpleMsg2(String msg) {
        System.out.println("topic模式 topic.* 接收到消息 :" + msg);
        //int i = 1 / 0;
        //消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
    }

    /**
     * 简单模式 消费者  //TODO 这个路由key为 topic 是收不到消息的  发送者的路由是:topic.demo
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue("topic.queue.demo3"), exchange = @Exchange(name = "topic.exchange.demo", type = ExchangeTypes.TOPIC), key = "topic"))
    public void simpleMsg3(String msg) {
        System.out.println("topic模式 topic 接收到消息 :" + msg);
        //int i = 1 / 0;
        //消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
    }

}
