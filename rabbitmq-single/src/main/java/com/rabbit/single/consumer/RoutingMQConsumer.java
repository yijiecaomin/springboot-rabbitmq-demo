package com.rabbit.single.consumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 路由模式 消费者
 *
 * @Author su
 * @Date 2022/7/21 17:52
 */
@Component
public class RoutingMQConsumer {

    /**
     * 广播模式 消费者1号
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "routing.queue.demo"), exchange = @Exchange(value = "routing.exchange.demo", type = ExchangeTypes.DIRECT),key = "routing.msg.1"))
    public void fanoutMsg(String msg) {
        System.out.println("路由模式 1号 接收到消息 :" + msg);
    }

    /**
     * 广播模式 消费者2号
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "routing.queue.demo2"), exchange = @Exchange(value = "routing.exchange.demo", type = ExchangeTypes.DIRECT),key = "routing.msg.2"))
    public void fanoutMsg2(String msg) {
        System.out.println("路由模式 2号 接收到消息 :" + msg);
    }
}
