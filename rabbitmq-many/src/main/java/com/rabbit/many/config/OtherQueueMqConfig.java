package com.rabbit.many.config;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * 其他数据源队列创建配置
 *
 * @Author su
 * @Date 2022/7/28 11:52
 */
@Configuration
public class OtherQueueMqConfig {

    @Resource(name = "twoRabbitAdmin")
    private RabbitAdmin rabbitAdmin;

    @Bean
    void EcuOperationLogQueueDirect() {
        //新队列
        Queue queue = QueueBuilder.durable("new.test").build();
        rabbitAdmin.declareQueue(queue);
        DirectExchange directExchange = new DirectExchange("testExchange",true,false);
        rabbitAdmin.declareExchange(directExchange);
        rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(directExchange).with("testRouting"));
    }
}
