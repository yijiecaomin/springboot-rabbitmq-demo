package com.rabbit.many.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * MQ properties
 *
 * @Author su
 * @Date 2022/7/22 11:37
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "spring.rabbitmq")
public class MQProperties {

    /**
     * 默认MQip
     */
    private String defaultHost;

    /**
     * 默认MQ端口
     */
    private Integer defaultPort;

    /**
     * 默认MQ用户名
     */
    private String defaultUsername;

    /**
     * 默认MQ密码
     */
    private String defaultPassword;

    /**
     * MQ2 ip
     */
    private String twoHost;

    /**
     * MQ2 端口
     */
    private Integer twoPort;

    /**
     * MQ2 用户名
     */
    private String twoUsername;

    /**
     * MQ2 密码
     */
    private String twoPassword;

}
