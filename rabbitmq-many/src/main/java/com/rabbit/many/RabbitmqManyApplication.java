package com.rabbit.many;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * RabbitMQ 多MQ 的demo项目
 *
 *
 *
 * direct：如果路由键匹配，则直接投递到对应的队列
 *
 * fanout：不处理路由键，向所有与之绑定的队列投递消息
 *
 * topic：处理路由键，按模式匹配，向符合规则的队列投递消息
 *
 * headers：允许匹配消息的header，而非路由键，除此之外，direct完全一致，但性能差很多，基本不用了。
 *
 *
 * TODO 需要重点注意
 *      1. 消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
 *      2. 当用注解 @RabbitListener 指向非默认数据源@Primary  containerFactory = "其他数据源"  注解不会自动创建新的队列,会报找不到队列的错误,暂时我只知道手动创建的解决方案 有其他的求评论 告知一下
 *         手动创建方案: 请查看 OtherQueueMqConfig 这个配置手动创建
 *         不会自动创建的例子:@RabbitListener(bindings = @QueueBinding(value = @Queue("新队列名"), exchange = @Exchange(name = "指定交换机", type = ExchangeTypes.TOPIC), key = "路由"), containerFactory = "其他数据源")
 *      3. 在创建工厂的时候一定要使用 @Primary 这个注解，RabbitMQ会根据类型注入所以需要一个默认的主数据源  不然会报错: required a single bean, but 2 were found
 *
 */
@SpringBootApplication
public class RabbitmqManyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqManyApplication.class, args);
    }

}
