package com.rabbit.many.consumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 工作模式 /简单模式 消费者  containerFactory指向那个工厂
 * <p>
 * TODO 需要重点注意
 *      1. 消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
 *      2. 当用注解 @RabbitListener 指向非默认数据源@Primary  containerFactory = "其他数据源"  注解不会自动创建新的队列,会报找不到队列的错误,暂时我只知道手动创建的解决方案 有其他的求评论 告知一下
 *          手动创建方案: 请查看 OtherQueueMqConfig 这个配置手动创建
 *          不会自动创建的例子:@RabbitListener(bindings = @QueueBinding(value = @Queue("新队列名"), exchange = @Exchange(name = "指定交换机", type = ExchangeTypes.TOPIC), key = "路由"), containerFactory = "其他数据源")
 * @Author su
 * @Date 2022/7/21 16:06
 */
@Component
public class TopicMQConsumer {

    /**
     * TOPIC模式 消费者  TODO 可以收到 #多个单词模糊匹配   发送者的路由是:topic.demo
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue("topic.queue.demo1"), exchange = @Exchange(name = "topic.exchange.#", type = ExchangeTypes.TOPIC), key = "topic.#"), containerFactory = "defaultFactory")
    public void simpleMsg1(String msg) {
        System.out.println("topic模式 topic.# 接收到消息 :" + msg);
        //int i = 1 / 0;
        //消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
    }

    /**
     * TOPIC模式 消费者  //TODO 可以收到 单个单词 *模糊匹配   发送者的路由是:topic.demo
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue("topic.queue.demo2"), exchange = @Exchange(name = "topic.exchange.demo", type = ExchangeTypes.TOPIC), key = "topic.*"), containerFactory = "defaultFactory")
    public void simpleMsg2(String msg) {
        System.out.println("topic模式 topic.* 接收到消息 :" + msg);
        //int i = 1 / 0;
        //消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
    }

    /**
     * TOPIC模式 消费者  //TODO 这个路由key为 topic 是收不到消息的  发送者的路由是:topic.demo
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue("topic.queue.demo3"), exchange = @Exchange(name = "topic.exchange.demo", type = ExchangeTypes.TOPIC), key = "topic"), containerFactory = "defaultFactory")
    public void simpleMsg3(String msg) {
        System.out.println("topic模式 topic 接收到消息 :" + msg);
        //int i = 1 / 0;
        //消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
    }
}
