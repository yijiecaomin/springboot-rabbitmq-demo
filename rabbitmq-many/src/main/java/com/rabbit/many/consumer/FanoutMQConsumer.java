package com.rabbit.many.consumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 广播模式 消费者
 *
 * TODO 需要重点注意
 *      1. 消费者消费消息的时候，发生异常情况，导致消息未确认，该消息会被重复消费(默认没有重复次数，即无限循环消费)，但可以通过设置重试次数以及达到重试次数之后的消息处理
 *      2. 当用注解 @RabbitListener 指向非默认数据源@Primary  containerFactory = "其他数据源"  注解不会自动创建新的队列,会报找不到队列的错误,暂时我只知道手动创建的解决方案 有其他的求评论 告知一下
 *         手动创建方案: 请查看 OtherQueueMqConfig 这个配置手动创建
 *         不会自动创建的例子:@RabbitListener(bindings = @QueueBinding(value = @Queue("新队列名"), exchange = @Exchange(name = "指定交换机", type = ExchangeTypes.TOPIC), key = "路由"), containerFactory = "其他数据源")
 *
 * @Author su
 * @Date 2022/7/21 17:52
 */
@Component
public class FanoutMQConsumer {

    /**
     * 广播模式 消费者1号
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "fanout.queue.demo"), exchange = @Exchange(value = "fanout.exchange.demo", type = ExchangeTypes.FANOUT)),containerFactory = "defaultFactory")
    public void fanoutMsg(String msg) {
        System.out.println("广播模式 1号 接收到消息 :" + msg);
    }

    /**
     * 广播模式 消费者2号
     *
     * @param msg
     */
    @RabbitHandler
    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "fanout.queue.demo2"), exchange = @Exchange(value = "fanout.exchange.demo", type = ExchangeTypes.FANOUT)),containerFactory = "defaultFactory")
    public void fanoutMsg2(String msg) {
        System.out.println("广播模式 2号 接收到消息 :" + msg);
    }
}
