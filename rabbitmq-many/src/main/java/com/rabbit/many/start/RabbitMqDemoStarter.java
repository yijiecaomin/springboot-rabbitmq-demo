package com.rabbit.many.start;

import com.rabbit.many.producer.FanoutMQProducer;
import com.rabbit.many.producer.RoutingMQProducer;
import com.rabbit.many.producer.SimpleWorkMQProducer;
import com.rabbit.many.producer.TopicMQProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 启动 发送MQ 的类
 *
 * @Author su
 * @Date 2022/7/21 16:14
 */
@Component
public class RabbitMqDemoStarter implements ApplicationRunner {

    @Autowired
    private SimpleWorkMQProducer simpleWorkMQProducer;
    @Autowired
    private FanoutMQProducer fanoutMQProducer;
    @Autowired
    private RoutingMQProducer routingMQProducer;
    @Autowired
    private TopicMQProducer topicMQProducer;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Simple---------------------------------");
        simpleWorkMQProducer.sendSimpleMq();

        Thread.sleep(5000);
        System.out.println("Work---------------------------------");
        simpleWorkMQProducer.sendWorkMq();

        Thread.sleep(5000);
        System.out.println("Fanout---------------------------------");
        fanoutMQProducer.sendFanoutMq();

        Thread.sleep(5000);
        System.out.println("Routing---------------------------------");
        routingMQProducer.sendRoutingMq();

        Thread.sleep(5000);
        System.out.println("Topic---------------------------------");
        topicMQProducer.sendTopicMq();
    }
}
