package com.rabbit.many.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 广播模式发送消息
 *
 * @Author su
 * @Date 2022/7/21 17:47
 */
@Component
public class FanoutMQProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Resource(name = "twoTemplate")
    private RabbitTemplate twoTemplate;

    public void sendFanoutMq() {
        for (int i = 0; i < 5; i++) {
            rabbitTemplate.convertAndSend("fanout.exchange.demo", null, "Fanout 消息来了 :" + i);
        }
    }
}
