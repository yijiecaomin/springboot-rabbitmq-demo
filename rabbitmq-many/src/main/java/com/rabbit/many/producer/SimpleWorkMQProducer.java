package com.rabbit.many.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 简单模式 / 工作模式 发送者
 *
 * @Author su
 * @Date 2022/7/21 15:55
 */
@Component
public class SimpleWorkMQProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 另个RabbitTemplate  需要用这个发送 就使用这个
     */
    @Resource(name = "twoTemplate")
    private RabbitTemplate twoTemplate;


    /**
     * 发送简单 MQ
     */
    public void sendSimpleMq() {
        for (int i = 1; i <= 3; i++) {
            rabbitTemplate.convertAndSend("simple.demo", "发送简单消息内容: 简单简单简单 :" + i);
        }

    }

    /**
     * 发送工作 MQ
     */
    public void sendWorkMq() {
        for (int i = 1; i <= 10; i++) {
            rabbitTemplate.convertAndSend("work.demo", "发送工作消息内容: 工作工作简单 :" + i);
        }
    }
}
