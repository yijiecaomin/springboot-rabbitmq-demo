package com.rabbit.many.producer.config;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 路由 配置类
 * 1.创建交换机
 * 2.绑定路由
 *
 * @Author su
 * @Date 2022/7/21 18:03
 */
@Configuration
public class RoutingRabbitMQConfig {

    /**
     * 向 RabbitMQ 上创建 交换机
     *
     * @return
     */
    @Bean
    public DirectExchange getDirectExchange() {
        //return new DirectExchange("routing.rabbit.demo",true,false);
        return ExchangeBuilder.directExchange("routing.exchange.demo").durable(true).build();
    }

}
